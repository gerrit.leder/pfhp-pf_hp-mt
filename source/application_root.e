note
	description: "Objects that ..."

	instructions: "[
		To build and install type:

			. build_and_install.sh
	]"

	copyright: "[
		Copyright (C) 2016-2017  Gerrit Leder, Finnian Reilly

		Gerrit Leder, Overather Str. 10, 51429 Bergisch-Gladbach, GERMANY
		gerrit.leder@gmail.com

		Finnian Reilly, Dunboyne, Co Meath, Ireland.
		finnian@eiffel-loop.com
	]"

	license: "Scroll down to end"

class
	APPLICATION_ROOT

inherit
	EL_MULTI_APPLICATION_ROOT [BUILD_INFO]

create
	make

feature {NONE} -- Implementation

	Application_types: ARRAY [TYPE [EL_SUB_APPLICATION]]
	--test : PF_HP_3_0_TEST_SET
			--
		once
			Result := <<
		--		{AUTOTEST_DEVELOPMENT_APP},
				{PF_HP_1_0_APP},
				{FS_PF_HP_APP},
		--		{PF_HP_2_1_APP},
		--		{PF_HP_2_0_APP},
				{MULTI_CORE_PF_HP_3_1_APP},
				{ONE_CORE_PF_HP_2_0_APP},
				{ONE_CORE_PF_HP_3_0_APP},
				{MULTI_CORE_PF_HP_2_1_APP}
			>>
		end

	notes: TUPLE [DONE_LIST, TO_DO_LIST]
		do
		end

note
	license: "[
		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
	]"

end
