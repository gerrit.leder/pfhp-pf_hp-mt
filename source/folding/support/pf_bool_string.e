note
	description: "Summary description for {PF_BOOL_STRING}."

	author: "Finnian Reilly"

	copyright: "[
		Copyright (C) 2016-2017  Gerrit Leder, Finnian Reilly

		Gerrit Leder, Overather Str. 10, 51429 Bergisch-Gladbach, GERMANY
		gerrit.leder@gmail.com

		Finnian Reilly, Dunboyne, Co Meath, Ireland.
		finnian@eiffel-loop.com
	]"

	license: "Scroll down to end"

class
	PF_BOOL_STRING

inherit
	BOOL_STRING

create
	make

feature -- Conversion

	to_string_32: STRING_32
		local
			i: INTEGER;
		do
			create Result.make (count)
			from i := 1 until i > count loop
				if item (i) then
					Result.append_code (0x25CF)
				else
					Result.append_code (0x25CE)
				end
				Result.append_character (' ')
				i := i + 1
			end
		end

	to_string_8: STRING_8
		local
			i: INTEGER;
		do
			create Result.make (count)
			from i := 1 until i > count loop
				if item (i) then
					Result.append_code (2)
				else
					Result.append_code (79)
				end
				Result.append_character (' ')
				i := i + 1
			end
		end

	to_general: READABLE_STRING_GENERAL
		do
			if {PLATFORM}.is_windows then
				Result := to_string_8
			else
				Result := to_string_32
			end
		end

note
	license: "[
		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
	]"

end
