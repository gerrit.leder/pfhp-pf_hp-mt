note
	description: "[
		Refactored utility methods from class PF_HP
		 using class FOLD_SEQUENCE
	]"
	author: "Gerrit Leder"

	copyright: "[
		Copyright (C) 2016-2017  Gerrit Leder, Finnian Reilly

		Gerrit Leder, Overather Str. 10, 51429 Bergisch-Gladbach, GERMANY
		gerrit.leder@gmail.com

		Finnian Reilly, Dunboyne, Co Meath, Ireland.
		finnian@eiffel-loop.com
	]"

	license: "Scroll down to end"
class
	FS_TOOL

inherit
	BOOLEAN_CONSTANTS

	DIRECTION_ROUTINES

feature {ANY} -- Initialization

	calc_losses (grid: FS_GRID; i: INTEGER; seq: PF_BOOL_STRING; fold: FOLD_SEQUENCE): INTEGER
			--check grid at position (i, i) set to (0, 0) for losses at each position of the folded sequence
			--Gives sum of losses at each position
		local
			loss: INTEGER
		do
			loss := 9999
			if grid.used.has (zero) then
				loss := grid.calc_losses (i, i, fold)
			end
			Result := loss
		end

	compare_loss_min_add_fold (grid: FS_GRID; loss, min: INTEGER; fold: FOLD_SEQUENCE; folds: FOLDS_LIST): FOLDS_LIST
			--Based on comparison of loss and minimal losses, eventually clear all folds, and add fold to folds
		require
			object_comparison: folds.object_comparison
		do
			Result := folds.twin
			if grid.used.has (zero) then
				if loss = min then
					Result.extend (fold.twin)
					check
						Result.has (fold.twin)
					end
				end
				if loss < min then
					create Result.make
					Result.extend (fold.twin)
					check
						Result.has (fold.twin)
					end
				end
			end
		end

	convert_str_bool_string (a_strseq: STRING): PF_BOOL_STRING
			--Given a bit string of values '0's and '1's return a sequence of values False and True
		local
			a_seq: PF_BOOL_STRING
			i: INTEGER
		do
			create a_seq.make (a_strseq.count)
			from
				i := 1
			until
				i = a_strseq.count
			loop
					--io.put_string (i.out)
					--io.put_string ("%N")
				i := i + 1
			end
			from
				i := 1
			until
				i = a_strseq.count + 1
			loop
					--io.put_string (i.out)
					--io.put_string ("%N")
				if a_strseq.item (i).is_equal ('1') then
					a_seq.put (True, i)
				else
					a_seq.put (False, i)
				end
				i := i + 1
			end
				--	io.put_string ("%N")
				--	io.put_string (a_fold + "%N")
			Result := a_seq
		end

	embed (grid: FS_GRID; i: INTEGER; seq: PF_BOOL_STRING; fold: FOLD_SEQUENCE): FS_GRID
			--Starting at position (i, i) set to (0, 0) of two-dimensional grid,
			--put values of sequence seq according to directions in fold.
		local
			--a_grid : GRID_2_0

		do
				--create a_grid.make (seq.count + 1)
				--	 io.put_string ("%N")
				--	 io.put_string (seq.to_string)
				--	 io.put_string ("%N")
			grid.embed (i, i, seq, fold)
				--a_grid := grid.twin

			Result := grid --was a_grid

				--	grid.print_grid

		end -- embedd

	generate_first_fold (count: INTEGER): FOLD_SEQUENCE
			--Give a string of the form 'N...N'
		local
			i: INTEGER
		do
			create Result.make (count - 1)
			from
				i := 1
			until
				i = count
			loop
				debug ("TOOL")
					io.put_string (i.out)
					io.put_new_line
				end
				Result.extend (N)
				i := i + 1
			end
		end

note
	license: "[
		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
	]"

end
