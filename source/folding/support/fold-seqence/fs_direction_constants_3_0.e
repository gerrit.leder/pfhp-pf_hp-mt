note
	description: "Constants for class FOLD_SEQUENCE"
	author: "Finnian Reilly"

	copyright: "[
		Copyright (C) 2016-2017  Gerrit Leder, Finnian Reilly

		Gerrit Leder, Overather Str. 10, 51429 Bergisch-Gladbach, GERMANY
		gerrit.leder@gmail.com

		Finnian Reilly, Dunboyne, Co Meath, Ireland.
		finnian@eiffel-loop.com
	]"

	license: "Scroll down to end"
class
	FS_DIRECTION_CONSTANTS_3_0

feature {NONE} -- Directions

	North: NATURAL_64 = 0b0
	East: NATURAL_64 = 0b1
	South: NATURAL_64 = 0b10
	West: NATURAL_64 = 0b11

	S: NATURAL_64 = 0b01

	S_S: NATURAL_64 = 0b01_01

	S_R: NATURAL_64 = 0b01_11

	S_L: NATURAL_64 = 0b01_10

	--S: NATURAL_64 = 0b010

	--S_E: NATURAL_64 = 0b010_011

	--S_W: NATURAL_64 = 0b010_100

	--S_S: NATURAL_64 = 0b010_010

	R: NATURAL_64 = 0b11

	R_S: NATURAL_64 = 0b11_01

	R_L: NATURAL_64 = 0b11_10

	R_R: NATURAL_64 = 0b11_11

	L: NATURAL_64 = 0b10

	L_R: NATURAL_64 = 0b10_11

	L_S: NATURAL_64 = 0b10_01

	L_L: NATURAL_64 = 0b10_10

feature {NONE} -- Constants

	Direction_mask: NATURAL_64	= 0b11_11

	Direction_first_mask: NATURAL_64	= 0b11
		-- masks out first direction in double direction

	Bit_count: INTEGER = 4;

note
	license: "[
		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
	]"

end
