note
	description: "[
		two-dimensional Grid as an array2 using class FOLD_SEQUENCE
	]"

	copyright: "[
		Copyright (C) 2016-2017  Gerrit Leder, Finnian Reilly

		Gerrit Leder, Overather Str. 10, 51429 Bergisch-Gladbach, GERMANY
		gerrit.leder@gmail.com

		Finnian Reilly, Dunboyne, Co Meath, Ireland.
		finnian@eiffel-loop.com
	]"

	license: "Scroll down to end"

class
	FS_GRID

inherit
	POINT_SET_CONSTANTS

	BOOLEAN_CONSTANTS

	FS_DIRECTION_CONSTANTS

create
	make

feature {NONE} -- Initialization

	make (i: INTEGER)
		do
				--create a.make_filled (zero, 2 * i, 2 * i) --i = seq_count+1
			create a.make (- i, i, - i, i)
			create used.make (- i, i, - i, i)
		end

feature -- Access

	used: BOOLEAN_GRID

feature -- Element change

	calc_losses (line, col: INTEGER; a_fold: FOLD_SEQUENCE): INTEGER
		local
			y, x, losses: INTEGER;
		do
			x := col; y := line
			losses := 0
			a_fold.start
			a.go_to (line, col); used.go_to (line, col)
			if a.item and then used.item then
				inspect a_fold.item_1
					when N then
						losses := losses + a.losses (Points_E_S_W)
						y := y - 1
					when S then
						losses := losses + a.losses (Points_N_E_W)
						y := y + 1
					when W then
						losses := losses + a.losses (Points_N_E_S)
						x := x - 1
					when E then
						losses := losses + a.losses (Points_N_S_W)
						x := x + 1
				else end
			elseif not a.item then
				inspect a_fold.item_1
					when N then
						y := y - 1
					when S then
						y := y + 1
					when W then
						x := x - 1
					when E then
						x := x + 1
				else end
			end
			from a_fold.start until a_fold.after loop
				a.go_to (y, x); used.go_to (y, x)
				if a.item and then used.item then
					inspect a_fold.item_pair
						when N_N then
							losses := losses + a.losses (Points_E_W)
							y := y - 1
						when N_W then
							losses := losses + a.losses (Points_N_E)
							x := x - 1
						when W_W then
							losses := losses + a.losses (Points_N_S)
							x := x - 1
						when E_N then
							losses := losses + a.losses (Points_E_S)
							y := y - 1
						when N_E then
							losses := losses + a.losses (Points_N_W)
							x := x + 1
						when W_N then
							losses := losses + a.losses (Points_S_W)
							y := y - 1
						when S_S then
							losses := losses + a.losses (Points_E_W)
							y := y + 1
						when E_S then
							losses := losses + a.losses (Points_N_E)
							y := y + 1
						when E_E then
							losses := losses + a.losses (Points_N_S)
							x := x + 1
						when S_W then
							losses := losses + a.losses (Points_E_S)
							x := x - 1
						when W_S then
							losses := losses + a.losses (Points_N_W)
							y := y + 1
						when S_E then
							losses := losses + a.losses (Points_S_W)
							x := x + 1

					else end
				elseif not a.item then
					inspect a_fold.item_1
						when N then
							y := y - 1
						when S then
							y := y + 1
						when E then
							x := x + 1
						when W then
							x := x - 1
					else end
				end
				a_fold.forth
			end
			a_fold.finish
			a.go_to (y, x); used.go_to (y, x)
			if a.item and then used.item then
				inspect a_fold.item_2
					when N then
						losses := losses + a.losses (Points_N_E_W)
					when S then
						losses := losses + a.losses (Points_E_S_W)
					when E then
						losses := losses + a.losses (Points_N_E_S)
					when W then
						losses := losses + a.losses (Points_N_S_W)

				else end
			end
			Result := losses
		end -- calc_losses

	embed (line, col: INTEGER; seq: BOOL_STRING; a_fold: FOLD_SEQUENCE)
		require
			--  line = 0
			--  col = 0
			--  not seq.is_equal (Void)
			--  not fold.is_equal(Void)

		local
			i, y, x: INTEGER
		do
			x := col; y := line
			a.put (seq [1], line, col)
			used.put (one, y, x)
			from i := 2; a_fold.start until a_fold.after loop
				inspect a_fold.item_1
					when N then
						y := y - 1
						if not (used.row_col_item (y, x)) then
							a.put (seq [i], y, x)
							used.put (one, y, x)
						else
							used.initialize (one)
						end
					when S then
						y := y + 1
						if not (used.row_col_item (y, x)) then
							a.put (seq [i], y, x)
							used.put (one, y, x)
						else
							used.initialize (one)
						end
					when E then
						x := x + 1
						if not (used.row_col_item (y, x)) then
							a.put (seq [i], y, x)
							used.put (one, y, x)
						else
							used.initialize (one)
						end
					when W then
						x := x - 1
						if not (used.row_col_item (y, x)) then
							a.put (seq [i], y, x)
							used.put (one, y, x)
						else
							used.initialize (one)
						end
				else end
				a_fold.forth
				i := i + 1
			end
		end

feature {NONE} -- Internal attributes

	a: BOOLEAN_GRID;

note
	license: "[
		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
	]"

end -- class GRID
