note
	description: "[
		PF_HP Ver 1.0: brute force proteinfolding in the 2D HP Model
		 using class FOLD_SEQUENCE
	]"

	copyright: "[
		Copyright (C) 2016-2017  Gerrit Leder, Finnian Reilly

		Gerrit Leder, Overather Str. 10, 51429 Bergisch-Gladbach, GERMANY
		gerrit.leder@gmail.com

		Finnian Reilly, Dunboyne, Co Meath, Ireland.
		finnian@eiffel-loop.com
	]"

	license: "Scroll down to end"

class
	FS_PF_HP

inherit
	PF_HP_COMMAND
		redefine
			make
		end

	BOOLEAN_CONSTANTS

	FS_DIRECTION_CONSTANTS

create
	make

feature {EL_COMMAND_LINE_SUB_APPLICATION} -- Initialization

	make (a_strseq: like strseq; a_output_path: like output_path)
		do
			Precursor (a_strseq, a_output_path)

			create tool
			minimum_loss := 9999; loss := 9999
			number := 0

			create fold_list.make
			--create losses.make
			--losses.compare_objects
			--create indices.make --from_collection(losses)

			create fold.make (strseq.count - 1) --was - 1
			create seq.make (strseq.count)

				--create zero.make

				--create Bit sequence seq:
			seq := tool.convert_str_bool_string (strseq)
				--create first fold:
			fold := tool.generate_first_fold (strseq.count)
			seq_count := seq.count

			create grid.make (seq_count + 1)
			grid := tool.embed (grid, 0, seq, fold)
			loss := tool.calc_losses (grid, 0, seq, fold)
			fold_list := tool.compare_loss_min_add_fold (grid, loss, minimum_loss, fold, fold_list)
		end

feature -- Basic operations

	gen_folds
		local
			k, j, fold_count: INTEGER; c: BOOLEAN
		do
			log.enter ("gen_folds")
			c := False; j := 0

			if loss < minimum_loss then
				minimum_loss := loss
				check fold_list.has (fold) end
			end
			from
				k := 0
			until
				j = 1
			loop
				fold_count := fold.count
				if fold_count > 1 then
					from
						k := fold_count - 1
						c := half_add (fold_count)
					until
						k = 1 --was 1
					loop
						if c then
							c := half_add (k)
						end
						k := k - 1
					end
				end
				if c then
					j := 1
				end
					--brute force enumeration of fold_list:
				if j = 0 then
					debug ("PF_HP")
						lio.put_string (fold.to_string)
						lio.put_new_line
					end

					create grid.make (seq_count + 1)
					grid := tool.embed (grid, 0, seq, fold)
					loss := tool.calc_losses (grid, 0, seq, fold)
					fold_list := tool.compare_loss_min_add_fold (grid, loss, minimum_loss, fold, fold_list)
					if loss < minimum_loss then
						minimum_loss := loss
						check fold_list.has (fold) end
						check fold_list.count = 1 end
					end
				end
			end
			log.exit
		end

	half_add (i: INTEGER): BOOLEAN
		do
			inspect fold.i_th (i)
				when N then
					fold.put_i_th (E, i)
				when E then
					fold.put_i_th (S, i)
				when S then
					fold.put_i_th (W, i)
				when W then
					fold.put_i_th (N, i)
					Result := True

			else end
		end

	print_folds
		do
			output.put_new_line
				--fold_list.fill_tagged_out_memory
				--io.put_string(fold_list.out)
			output.put_new_line
		end -- print_folds

	print_indices
		do
			output.put_new_line
				-- indices.fill_tagged_out_memory
				--io.put_string(indices)
			output.put_new_line
		end -- print_indices

	print_item (item: like fold)
		do
			number := number + 1
			output.put_character_8 ('#')
			output.put_integer (number)
			output.put_character_8 (' ')
			output.put_string (item.to_string)
			output.put_character_8 ('%N')
		end -- print_item

	print_losses
		do
			output.put_new_line
				--losses.fill_tagged_out_memory
				--io.put_string(losses)
			output.put_new_line
		end -- print_losses

	print_min_fold
		do
			fold_list.do_all (agent print_item)
		end

feature {NONE} -- Implementation

	fold_as_string: STRING
		do
			Result := fold.to_string
		end

feature {NONE} -- Internal attributes

	fold: FOLD_SEQUENCE

	fold_list: FOLDS_LIST

	--losses: LINKED_LIST [INTEGER];

	grid: FS_GRID

	iseq: INTEGER

	number: INTEGER
		--Constructor:

	seq: PF_BOOL_STRING

	seq_count: INTEGER

	loss: INTEGER

	tool: FS_TOOL;

note
	license: "[
		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
	]"

end
