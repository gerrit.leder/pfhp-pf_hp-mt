note
	description: "Generate direction constants code"

	author: "Finnian Reilly"

	copyright: "[
		Copyright (C) 2016-2017  Gerrit Leder, Finnian Reilly

		Gerrit Leder, Overather Str. 10, 51429 Bergisch-Gladbach, GERMANY
		gerrit.leder@gmail.com

		Finnian Reilly, Dunboyne, Co Meath, Ireland.
		finnian@eiffel-loop.com
	]"

	license: "Scroll down to end"
class
	DIRECTION_ROUTINES

inherit
	FS_DIRECTION_CONSTANTS

feature {NONE} -- Implementation

	direction_letter (a_item: NATURAL_64): CHARACTER
		do
			inspect a_item
				when N then
					Result := 'N'
				when S then
					Result := 'S'
				when E then
					Result := 'E'
				when W then
					Result := 'W'
			else
				Result := '?'
			end
		end

	letter_as_natural_64 (d: CHARACTER): NATURAL_64
		do
			inspect d
				when 'N' then
					Result := N
				when 'S' then
					Result := S
				when 'E' then
					Result := E
				when 'W' then
					Result := W
			else
			end
		end

note
	license: "[
		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
	]"

end
