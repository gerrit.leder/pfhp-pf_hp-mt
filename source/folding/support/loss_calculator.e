note
	description: "Summary description for {LOSS_CALCULATOR}."

	author: "Finnian Reilly"

	copyright: "[
		Copyright (C) 2016-2017  Gerrit Leder, Finnian Reilly

		Gerrit Leder, Overather Str. 10, 51429 Bergisch-Gladbach, GERMANY
		gerrit.leder@gmail.com

		Finnian Reilly, Dunboyne, Co Meath, Ireland.
		finnian@eiffel-loop.com
	]"

	license: "Scroll down to end"

class
	LOSS_CALCULATOR

create
	make

feature {NONE} -- Initialization

	make (a_grid: like grid; delta_x, delta_y: INTEGER; a_point_set: like point_set)
		do
			grid := a_grid; point_set := a_point_set
			if delta_x = 0 then
				delta := delta_x
				is_x_delta := True
			else
				delta := delta_y
			end
		end

feature -- Access

	delta: INTEGER

	is_x_delta: BOOLEAN

	losses: INTEGER

feature -- Status query

	is_default: BOOLEAN
		do
		end

feature -- Basic operations

	find_losses
		do
			losses := grid.losses (point_set)
		end

feature {NONE} -- Internal attributes

	grid: BOOLEAN_GRID

	point_set: NATURAL;

note
	license: "[
		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
	]"


end
