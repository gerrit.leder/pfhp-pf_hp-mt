note
	description: "Implementation of PF_HP_2_0 using one core (thread)"
	author: "Finnian Reilly"

	copyright: "[
		Copyright (C) 2016-2017  Gerrit Leder, Finnian Reilly

		Gerrit Leder, Overather Str. 10, 51429 Bergisch-Gladbach, GERMANY
		gerrit.leder@gmail.com

		Finnian Reilly, Dunboyne, Co Meath, Ireland.
		finnian@eiffel-loop.com
	]"

	license: "Scroll down to end"

class
	ONE_CORE_PF_HP_2_0 [G -> GRID_2_X create make end]

inherit
	PF_HP_2_0 [G]

create
	make

feature {NONE} -- Implemenation

	calc_losses (a_fold: like new_fold; iteration_count: NATURAL_32)
		do
			print_progress (iteration_count)
			grid.calculate (a_fold)
			update_minimum_loss (a_fold)
--			log_losses (fold_string (a_fold), fold.grid_used_has_zero, fold.losses, minimum_loss, fold_list.count)
		end

	gen_folds
		do
			log.enter ("gen_folds")
			fold.permute (Current)
			log.exit
		end

	initialize
		do
			fold := new_fold; grid := new_grid
			grid.calculate (fold)
			calc_losses (fold, 1)
		end

feature {NONE} -- Internal attributes

	fold: like new_fold

	grid: GRID_2_X;

note
	license: "[
		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
	]"

end
