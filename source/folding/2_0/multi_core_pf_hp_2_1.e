note
	description: "Implementation of PF_HP_2_0 using many cores (threads)"
	author: "Finnian Reilly"

	copyright: "[
		Copyright (C) 2016-2017  Gerrit Leder, Finnian Reilly

		Gerrit Leder, Overather Str. 10, 51429 Bergisch-Gladbach, GERMANY
		gerrit.leder@gmail.com

		Finnian Reilly, Dunboyne, Co Meath, Ireland.
		finnian@eiffel-loop.com
	]"

	license: "Scroll down to end"

class
	MULTI_CORE_PF_HP_2_1 [G -> GRID_2_X create make end]

inherit
	ONE_CORE_PF_HP_2_0 [G]
		rename
			make as make_pf
		redefine
			gen_folds, Iterations_per_dot, new_fold
		end

	EL_MODULE_LOGGING

create
	make, make_pf

feature {EL_COMMAND_LINE_SUB_APPLICATION} -- Initialization

	make (a_strseq: like strseq; a_output_path: like output_path; a_max_thread_count: INTEGER)
		do
			max_thread_count := a_max_thread_count
			make_pf (a_strseq, a_output_path)

			create pool.make (max_thread_count)
		end

feature {NONE} -- Implementation

	gen_folds
		local
			done_list: ARRAYED_LIST [like new_folder]
			distributer: like new_distributer
			iteration_count: NATURAL_32; folder: like new_folder
		do
			log.enter ("gen_folds")
			distributer := new_distributer
			create done_list.make (11)

			from until fold.is_last_north loop
				if pool.is_empty then
					folder := new_folder
				else
					folder := pool.item
					pool.remove
				end
				folder.fold.set_data (fold)
				distributer.wait_apply (agent folder.gen_folds)
				distributer.collect (done_list)
				merge_minimum_losses (done_list)

				fold.partial_permute
				iteration_count := iteration_count + 1
				print_progress (iteration_count)
			end
			distributer.do_final
			distributer.collect_final (done_list)
			merge_minimum_losses (done_list)
			log.exit
		end

	merge_minimum_losses (list: ARRAYED_LIST [like new_folder])
		local
			l_fold_list: like fold_list
		do
			from list.start until list.after loop
				l_fold_list := list.item.fold_list
				from l_fold_list.start until l_fold_list.after loop
					if attached {LIMITED_FOLD_ARRAY} l_fold_list.item as l_fold then
						if l_fold.grid_used_has_zero and then l_fold.losses <= minimum_loss and then not fold_list.has (l_fold) then
							update_minimum_loss (l_fold)
						end
--						if Logging.is_active then
--							log_losses (fold_string (l_fold), l_fold.grid_used_has_zero, l_fold.losses, minimum_loss, fold_list.count)
--						end
					end
					l_fold_list.forth
				end
				pool.put (list.item)
				list.remove
			end
		end

feature {NONE} -- Factory

	new_distributer: EL_PROCEDURE_DISTRIBUTER [like new_folder]
		do
--			if Logging.is_active then
--				create {EL_LOGGED_PROCEDURE_DISTRIBUTER [like new_folder]} Result.make (max_thread_count)
--			else
				create Result.make (max_thread_count)
--			end
		end

	new_fold: LIMITED_FOLD_ARRAY
		do
			create Result.make (strseq)
		end

	new_folder: LIMITED_ONE_CORE_PF_HP_2_0 [G]
		do
			create Result.make (strseq, output_path)
		end

feature {NONE} -- Internal attributes

	max_thread_count: INTEGER;

	pool: ARRAYED_STACK [like new_folder]

feature {NONE} -- Constants

	Iterations_per_dot: NATURAL_32
		once
			Result := 20
		end

note
	license: "[
		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
	]"

end
