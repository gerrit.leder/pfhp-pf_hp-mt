note
	description: "Summary description for {GRID_2_3}."
	author: "Finnian Reilly"

	copyright: "[
		Copyright (C) 2016-2017  Gerrit Leder, Finnian Reilly

		Gerrit Leder, Overather Str. 10, 51429 Bergisch-Gladbach, GERMANY
		gerrit.leder@gmail.com

		Finnian Reilly, Dunboyne, Co Meath, Ireland.
		finnian@eiffel-loop.com
	]"

	license: "Scroll down to end"

class
	GRID_2_3

inherit
	GRID_2_2
		redefine
			losses
		end

create
	make

feature -- Measurement

	losses (a_fold: SPECIAL [NATURAL_8]): INTEGER
		local
			i, y, x: INTEGER
		do
			a.go_to (0, 0); used.go_to (0, 0)
			if a.item and then used.item then
				inspect a_fold [0]
					when N then
						Result := Result + a.losses (Points_E_S_W)
						y := y - 1
					when S then
						Result := Result + a.losses (Points_N_E_W)
						y := y + 1
					when W then
						Result := Result + a.losses (Points_N_E_S)
						x := x - 1
					when E then
						Result := Result + a.losses (Points_N_S_W)
						x := x + 1
				else end
			elseif not a.item then
				inspect a_fold [0]
					when N then
						y := y - 1
					when S then
						y := y + 1
					when W then
						x := x - 1
				else
					x := x + 1
				end
			end
			from i := 1 until i = a_fold.count loop
				a.go_to (y, x); used.go_to (y, x)
				if a.item and then used.item then
					inspect (a_fold [i - 1] |<< 3) | a_fold [i]
						when N_N then
							Result := Result + a.losses (Points_E_W)
							y := y - 1
						when N_W then
							Result := Result + a.losses (Points_N_E)
							x := x - 1
						when W_W then
							Result := Result + a.losses (Points_N_S)
							x := x - 1
						when E_N then
							Result := Result + a.losses (Points_E_S)
							y := y - 1
						when N_E then
							Result := Result + a.losses (Points_N_W)
							x := x + 1
						when W_N then
							Result := Result + a.losses (Points_S_W)
							y := y - 1
						when S_S then
							Result := Result + a.losses (Points_E_W)
							y := y + 1
						when E_S then
							Result := Result + a.losses (Points_N_E)
							y := y + 1
						when E_E then
							Result := Result + a.losses (Points_N_S)
							x := x + 1
						when S_W then
							Result := Result + a.losses (Points_E_S)
							x := x - 1
						when W_S then
							Result := Result + a.losses (Points_N_W)
							y := y + 1
						when S_E then
							Result := Result + a.losses (Points_S_W)
							x := x + 1
					else
					end

				elseif not a.item then
					inspect a_fold [i]
						when N then
							y := y - 1
						when S then
							y := y + 1
						when E then
							x := x + 1
					else
						x := x - 1
					end
				end
				i := i + 1
			end

			i := a_fold.count - 1 --was + 1
			a.go_to (y, x); used.go_to (y, x)
			if a.item and then used.item then
				inspect a_fold [i]
					when N then
						Result := Result + a.losses (Points_N_E_W)
					when S then
						Result := Result + a.losses (Points_E_S_W)
					when E then
						Result := Result + a.losses (Points_N_E_S)
				else
					Result := Result + a.losses (Points_N_S_W)
				end
			end
		end

note
	license: "[
		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
	]"

end
