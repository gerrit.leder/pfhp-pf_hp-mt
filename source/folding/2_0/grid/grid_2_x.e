note
	description: "[
		two-dimensional Grid as an array2
		
		Base class GRID_2_* classes
	]"

	copyright: "[
		Copyright (C) 2016-2017  Gerrit Leder, Finnian Reilly

		Gerrit Leder, Overather Str. 10, 51429 Bergisch-Gladbach, GERMANY
		gerrit.leder@gmail.com

		Finnian Reilly, Dunboyne, Co Meath, Ireland.
		finnian@eiffel-loop.com
	]"

	license: "Scroll down to end"

deferred class
	GRID_2_X

inherit
	POINT_SET_CONSTANTS

	BOOLEAN_CONSTANTS

	DIRECTION_CONSTANTS

feature {NONE} -- Initialization

	make (a_sequence: like sequence)
		do
			sequence := a_sequence
		end

feature -- Basic operations

	calculate (fold: FOLD_ARRAY)
		-- calculate losses
		do
			reset
			embed (sequence, fold.area)
			if used_has_zero then
				fold.set_losses (losses (fold.area))
				fold.set_grid_used_has_zero (True)
			else
				fold.set_losses (9999)
				fold.set_grid_used_has_zero (False)
			end
		end

feature {NONE} -- Implementation

	losses (a_fold: SPECIAL [NATURAL_8]): INTEGER
		deferred
		end

	embed (seq: BOOL_STRING; a_fold: SPECIAL [NATURAL_8])
		deferred
		end

	reset
		deferred
		end

	used_has_zero: BOOLEAN
		deferred
		end

feature {NONE} -- Internal attributes

	sequence: PF_BOOL_STRING;

note
	license: "[
		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
	]"

end
