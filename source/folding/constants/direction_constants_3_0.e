note
	description: "Summary description for {DIRECTION_CONSTANTS}."
	author: "Finnian Reilly"

	copyright: "[
		Copyright (C) 2016-2017  Gerrit Leder, Finnian Reilly

		Gerrit Leder, Overather Str. 10, 51429 Bergisch-Gladbach, GERMANY
		gerrit.leder@gmail.com

		Finnian Reilly, Dunboyne, Co Meath, Ireland.
		finnian@eiffel-loop.com
	]"

	license: "Scroll down to end"
class
	DIRECTION_CONSTANTS_3_0

feature {NONE} -- Directions


	S: NATURAL_8 = 0b1

	L: NATURAL_8 = 0b10

	R: NATURAL_8 = 0b11

	North: NATURAL_8 = 0b0
	East: NATURAL_8 = 0b1
	South: NATURAL_8 = 0b10
	West: NATURAL_8 = 0b11


feature {NONE} -- Directions combined

	S_S: NATURAL_8 = 0b0101

	S_L: NATURAL_8 = 0b0110

	S_R: NATURAL_8 = 0b0111

	--S_S: NATURAL_8 = 0b010010

	--S_E: NATURAL_8 = 0b010011

	--S_W: NATURAL_8 = 0b010100

	R_S: NATURAL_8 = 0b1101

	R_L: NATURAL_8 = 0b1110

	R_R: NATURAL_8 = 0b1111

	L_S: NATURAL_8 = 0b1001

	L_R: NATURAL_8 = 0b1011

	L_L: NATURAL_8 = 0b1010;

note
	license: "[
		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
	]"

end
