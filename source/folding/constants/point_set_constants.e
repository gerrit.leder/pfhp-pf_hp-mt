note
	description: "Sets of points around current grid position"

	author: "Finnian Reilly"

	copyright: "[
		Copyright (C) 2016-2017  Gerrit Leder, Finnian Reilly

		Gerrit Leder, Overather Str. 10, 51429 Bergisch-Gladbach, GERMANY
		gerrit.leder@gmail.com

		Finnian Reilly, Dunboyne, Co Meath, Ireland.
		finnian@eiffel-loop.com
	]"

	license: "Scroll down to end"

class
	POINT_SET_CONSTANTS

feature {NONE} -- Constants

	Point_N: NATURAL = 0b1000

	Points_E_S: NATURAL = 0b110

	Points_E_S_W: NATURAL = 0b111

	Points_E_W: NATURAL = 0b101

	Points_N_E: NATURAL = 0b1100

	Points_N_E_S: NATURAL = 0b1110

	Points_N_E_W: NATURAL = 0b1101

	Points_N_S: NATURAL = 0b1010

	Points_N_S_W: NATURAL = 0b1011

	Points_N_W: NATURAL = 0b1001

	Points_S_W: NATURAL = 0b11

note
	license: "[
		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
	]"
end
