note
	description: "[
		As the name implies, each constant describes a set of 4 positions around a center point.
		Going from the high to low the bits represent the boolean conditions

		4. North in the set
		3. East in the set
		2. South in the set
		1. West in the set

		So for example `Points_N_S_W' contains the points N, S and W but not E.
	]"
	author: "Finnian Reilly"

	copyright: "[
		Copyright (C) 2016-2017  Gerrit Leder, Finnian Reilly

		Gerrit Leder, Overather Str. 10, 51429 Bergisch-Gladbach, GERMANY
		gerrit.leder@gmail.com

		Finnian Reilly, Dunboyne, Co Meath, Ireland.
		finnian@eiffel-loop.com
	]"

	license: "Scroll down to end"
class
	DIRECTION_CONSTANTS

feature {NONE} -- Directions

	N: NATURAL_8 = 0b1

	S: NATURAL_8 = 0b10

	E: NATURAL_8 = 0b11

	W: NATURAL_8 = 0b100

feature {NONE} -- Directions combined

	N_N: NATURAL_8 = 0b001001

	N_E: NATURAL_8 = 0b001011

	N_W: NATURAL_8 = 0b001100

	S_S: NATURAL_8 = 0b010010

	S_E: NATURAL_8 = 0b010011

	S_W: NATURAL_8 = 0b010100

	E_E: NATURAL_8 = 0b011011

	E_N: NATURAL_8 = 0b011001

	E_S: NATURAL_8 = 0b011010

	W_W: NATURAL_8 = 0b100100

	W_N: NATURAL_8 = 0b100001

	W_S: NATURAL_8 = 0b100010;

note
	license: "[
		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
	]"

end
