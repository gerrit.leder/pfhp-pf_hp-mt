note
	description: "Summary description for {LOGGING_CONSTANTS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	LOGGING_ROUTINES

inherit
	EL_MODULE_LOG

feature {NONE} -- Implementation

	log_losses (fold: STRING; grid_used_has_zero: BOOLEAN; fold_loss, mininum_loss, fold_list_count: INTEGER)
		do
			if grid_used_has_zero then
				log.put_labeled_string (fold, grid_used_has_zero.out)
				log.put_integer_field (once " fold_loss", fold_loss)
				log.put_integer_field (once " mininum_loss", mininum_loss)
				log.put_integer_field (once " fold_list.count", fold_list_count)
				log.put_new_line
			end
		end
end
