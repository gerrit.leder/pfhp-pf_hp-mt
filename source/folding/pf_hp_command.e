note
	description: "Summary description for {PF_HP_I}."
	author: "Finnian Reilly"

	copyright: "[
		Copyright (C) 2016-2017  Gerrit Leder, Finnian Reilly

		Gerrit Leder, Overather Str. 10, 51429 Bergisch-Gladbach, GERMANY
		gerrit.leder@gmail.com

		Finnian Reilly, Dunboyne, Co Meath, Ireland.
		finnian@eiffel-loop.com
	]"

	license: "Scroll down to end"

deferred class
	PF_HP_COMMAND

inherit
	EL_COMMAND

	EL_MODULE_FILE_SYSTEM

	LOGGING_ROUTINES

feature {EL_COMMAND_CLIENT} -- Initialization

	make (a_strseq: like strseq; a_output_path: like output_path)
		require
			valid_count: a_strseq.count >= 3
		do
			strseq := a_strseq
			strseq.compare_objects
			output_path := a_output_path.twin
			if not output_path.base.ends_with_general ("txt") then
				output_path.append_step ("folds-" + strseq + ".txt")
			end
			File_system.make_directory (output_path.parent)
			create output.make_with_name (output_path)
			create crc
		end

feature -- Access

	output_path: EL_FILE_PATH

	strseq: STRING

feature -- Basic operations

	execute
		do
			log.enter ("execute")
			lio.put_labeled_string ("Calculating folds for HP sequence", seq.to_general)
			lio.put_new_line

			gen_folds

			if crc.checksum > 0 then
				log.put_labeled_string ("Calculation checksum", crc.checksum.out)
				log.put_new_line
			end

			log.put_labeled_string ("output.path", output.path.out)
			log.put_new_line
			output.open_write

			output.put_string_32 ({STRING_32}"Optimal fold(s) for " + seq.to_string_32 + ":")
			output.put_new_line

			print_min_fold

			output.put_new_line
			output.put_string ("%NMinimal losses: ")
			output.put_integer (minimum_loss)

			output.close
			log.exit
		end

feature {NONE} -- Implementation

	gen_folds
		deferred
		end

	print_min_fold
		deferred
		end

	print_progress (iteration_count: NATURAL_32)
		do
			if iteration_count \\ Iterations_per_dot = 0 then
				dot_count := dot_count + 1
				lio.put_character ('.')
				if dot_count \\ 100 = 0 then
					lio.put_new_line
				end
			end
		end

feature {NONE} -- Internal attributes

	seq: PF_BOOL_STRING
		deferred
		end

	minimum_loss: INTEGER

	output: EL_PLAIN_TEXT_FILE

	crc: EL_CYCLIC_REDUNDANCY_CHECK_32

	dot_count: NATURAL_32

feature {NONE} -- Constants

	Iterations_per_dot: NATURAL_32
		once
			Result := 100_000
		end

note
	license: "[
		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
	]"

end
