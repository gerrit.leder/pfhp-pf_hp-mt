note
	description: "Refactored utility methods from class PF_HP"
	author: "Gerrit Leder"

	copyright: "[
		Copyright (C) 2016-2017  Gerrit Leder, Finnian Reilly

		Gerrit Leder, Overather Str. 10, 51429 Bergisch-Gladbach, GERMANY
		gerrit.leder@gmail.com

		Finnian Reilly, Dunboyne, Co Meath, Ireland.
		finnian@eiffel-loop.com
	]"

	license: "Scroll down to end"

class
	TOOL_3_0

create
	make

feature {ANY} -- Initialization

	calc_losses (grid: GRID_3_5; i: INTEGER; seq: PF_BOOL_STRING; fold: FOLD_SEQUENCE_3_0): INTEGER
			--check grid at position (i, i) set to (0, 0) for losses at each position of the folded sequence
			--Gives sum of losses at each position
		local
			loss: INTEGER
		do
			loss := 9999
			if grid.used_has_zero then
				loss := grid.losses (fold.area)
			end
			Result := loss
		end

	compare_loss_min_add_fold (grid: GRID_3_5; loss, min: INTEGER; fold: FOLD_SEQUENCE_3_0; folds: FOLDS_LIST_3_0): FOLDS_LIST_3_0
			--Based on comparison of loss and minimal losses, eventually clear all folds, and add fold to folds
		local
			local_folds: FOLDS_LIST_3_0
		do
			create local_folds.make
			local_folds.compare_objects
			local_folds := folds.twin
			if grid.used_has_zero then
				if loss = min then
					local_folds.extend (fold.twin)
						--local_folds := folds.twin
					check
						local_folds.has (fold.twin)
					end
						--losses.extend (loss)
				end
				if loss < min then
						--create folds.make
						--create losses.make
					create local_folds.make
					local_folds.compare_objects
					local_folds.extend (fold.twin)
					check
						local_folds.has (fold.twin)
					end
						--losses.extend (loss)
						--min := loss
				end
			end
			Result := local_folds
		end

	convert_str_bool_string (a_strseq: STRING): PF_BOOL_STRING
			--Given a bit string of values '0's and '1's return a sequence of values False and True
		local
			a_seq: PF_BOOL_STRING
			i: INTEGER
		do
			create a_seq.make (a_strseq.count)
			from
				i := 1
			until
				i = a_strseq.count
			loop
					--io.put_string (i.out)
					--io.put_string ("%N")
				i := i + 1
			end
			from
				i := 1
			until
				i = a_strseq.count + 1
			loop
					--io.put_string (i.out)
					--io.put_string ("%N")
				if a_strseq.item (i).is_equal ('1') then
					a_seq.put (True, i)
				else
					a_seq.put (False, i)
				end
				i := i + 1
			end
				--	io.put_string ("%N")
				--	io.put_string (a_fold + "%N")
			Result := a_seq
		end


	generate_first_fold (strseq: STRING): STRING
			--Give a string of the form 'S...S'
		local
			i: INTEGER
			fold: STRING
		do
			create fold.make (strseq.count - 1)
			fold.compare_objects
			from
				i := 1
			until
				i = strseq.count
			loop
				debug ("TOOL")
					io.put_string (i.out)
					io.put_new_line
				end
				fold.append_character ('S')
				i := i + 1
			end
			Result := fold
		end

	make
			-- Initialization for `Current'.
		do
			zero := False
			one := True
		end

	zero, one: BOOLEAN;
		--The values False and True

note
	license: "[
		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
	]"

end
