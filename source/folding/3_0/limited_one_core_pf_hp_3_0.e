note
	description: "[
		Caculates minimum losses for a partial set of fold direction permutations.
		These limited permutations can be assigned to a thread for losses caculations.
	]"
	author: "Finnian Reilly"

	copyright: "[
		Copyright (C) 2016-2017  Gerrit Leder, Finnian Reilly

		Gerrit Leder, Overather Str. 10, 51429 Bergisch-Gladbach, GERMANY
		gerrit.leder@gmail.com

		Finnian Reilly, Dunboyne, Co Meath, Ireland.
		finnian@eiffel-loop.com
	]"

	license: "Scroll down to end"

class
	LIMITED_ONE_CORE_PF_HP_3_0 [G -> GRID_3_X create make end]

inherit
	ONE_CORE_PF_HP_3_0 [G]
		export
			{MULTI_CORE_PF_HP_3_1} fold, fold_list, minimum_loss
		redefine
			new_fold, gen_folds, calc_losses, print_progress
		end

create
	make

feature -- Basic operations

	gen_folds
		do
			fold.permute (Current)
		end

feature {NONE} -- Implementation

	new_fold: LIMITED_FOLD_ARRAY_3_0
		do
			create Result.make (strseq)
		end

	calc_losses (a_fold: like new_fold; iteration_count: NATURAL_32)
		do
			grid.calculate (a_fold)
			update_minimum_loss (a_fold)
		end

	print_progress (iteration_count: NATURAL_32)
		do
		end

note
	license: "[
		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
	]"
end
