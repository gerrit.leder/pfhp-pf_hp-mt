note
	description: "Same as GRID_2_3 but using nested inspect for double direction loss calc."
	author: "Finnian Reilly"
	copyright: "[
		Copyright (C) 2016-2017  Gerrit Leder, Finnian Reilly

		Gerrit Leder, Overather Str. 10, 51429 Bergisch-Gladbach, GERMANY
		gerrit.leder@gmail.com

		Finnian Reilly, Dunboyne, Co Meath, Ireland.
		finnian@eiffel-loop.com
	]"

	license: "Scroll down to end"

class
	GRID_3_5

inherit

	GRID_3_X
		redefine
			make
		end

create
	make

feature {NONE} -- Initialization

	make (a_sequence: like sequence)
		local
			i: INTEGER
		do
			Precursor (a_sequence)
			i := a_sequence.count + 1
			create grid_array.make (- i, i, - i, i)
			create used.make (- i, i, - i, i)
		end


--inherit
--	GRID_3_2
--		redefine
--			losses
--		end


feature -- Measurement

	losses (a_fold: SPECIAL [NATURAL_64]): INTEGER
		do
			Result := grid_array.fold_losses (a_fold, used)
		end

feature -- Element change

	embed (seq: BOOL_STRING; a_fold: SPECIAL [NATURAL_64])
		local
			i, y, x: INTEGER
			direction: NATURAL_64
		do
			y := 0
			x := 0
			grid_array.put (seq [1], y, x)
			used.put (one, y, x)
			direction := North
			from
				i := 0
			until
				i = a_fold.count
			loop
				inspect direction
				when North then
					inspect a_fold [i]
					when S then
						direction := North
						y := y - 1
					when R then
						x := x + 1
						direction := East
					when L then
						x := x - 1
						direction := West
					else
					end
				when East then
					inspect a_fold [i]
					when S then
						direction := East
						x := x + 1
					when R then
						y := y + 1
						direction := South
					when L then
						y := y - 1
						direction := North
					else
					end
				when South then
					inspect a_fold [i]
					when S then
						direction := South
						y := y + 1
					when R then
						x := x - 1
						direction := West
					when L then
						x := x + 1
						direction := East
					else
					end
				when West then
					inspect a_fold [i]
					when S then
						direction := West
						x := x - 1
					when R then
						y := y - 1
						direction := North
					when L then
						y := y + 1
						direction := South
					else
					end
				else
				end
				if not (used.row_col_item (y, x)) then
					grid_array.put (seq [i + 2], y, x)
					used.put (one, y, x)
				else
					used.initialize (one)
				end
				i := i + 1
			end
		end

feature --Queries

	used_has_zero: BOOLEAN
		do
			Result := used.has (zero)
		end

feature {NONE} -- Implementation

	reset
		do
			used.reset;
			grid_array.reset
		end

feature {NONE} -- Internal attributes

	grid_array: BOOLEAN_GRID_3_0

	used: BOOLEAN_GRID_3_0;

note
	license: "[
		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received grid_array copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
	]"

end
