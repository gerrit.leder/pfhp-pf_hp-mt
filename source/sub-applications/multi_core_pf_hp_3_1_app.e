note
	description: "[
		PF_HP Ver 1.0: brute force proteinfolding in the 2D HP Model
		Multi-core model (threads)
	]"

	usage: "[
		pf_hp -pf2_mt [-logging] [-threads <number of threads>] [-sequence <protein sequence as binary number>] [-out <output path>]
	]"

	copyright: "[
		Copyright (C) 2016-2017  Gerrit Leder, Finnian Reilly

		Gerrit Leder, Overather Str. 10, 51429 Bergisch-Gladbach, GERMANY
		gerrit.leder@gmail.com

		Finnian Reilly, Dunboyne, Co Meath, Ireland.
		finnian@eiffel-loop.com
	]"

	license: "Scroll down to end"

class
	MULTI_CORE_PF_HP_3_1_APP

inherit
	PF_HP_SUB_APPLICATION [MULTI_CORE_PF_HP_3_1 [GRID_3_5]]
		redefine
			 default_make, Option_name, argument_specs, new_command
		end

create
	make

feature {NONE} -- Implementation

	argument_specs: ARRAY [like specs.item]
		local
			list: ARRAYED_LIST [like specs.item]
		do
			create list.make_from_array (Precursor)
			list.extend (optional_argument ("threads", "Maximum number of threads to use"))
			Result := list.to_array
		end

	default_make: PROCEDURE
		do
			Result := agent {like command}.make (Default_sequence, default_output_path, 4)
		end

	new_command (sequence: STRING; output_path: EL_FILE_PATH): like command
		do
			create Result.make (sequence, output_path, 8)
		end

feature {NONE} -- Constants

	Checksum: NATURAL = 2371624897 --2676105973

	Description: STRING = "Test optimised calculation of HP sequences in two-dimensional grid using many threads"

	Log_filter: ARRAY [like CLASS_ROUTINES]
			--
		do
			Result := <<
				[{MULTI_CORE_PF_HP_3_1_APP}, All_routines],
				[{like command}, All_routines]
			>>
		end

	Option_name: STRING = "pf3_mt"

note
	license: "[
		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
	]"

end
