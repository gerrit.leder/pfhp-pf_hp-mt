note
	description: "Summary description for {PF_HP_SUB_APPLICATION}."
	author: "Finnian Reilly"

	copyright: "[
		Copyright (C) 2016-2017  Gerrit Leder, Finnian Reilly

		Gerrit Leder, Overather Str. 10, 51429 Bergisch-Gladbach, GERMANY
		gerrit.leder@gmail.com

		Finnian Reilly, Dunboyne, Co Meath, Ireland.
		finnian@eiffel-loop.com
	]"

	license: "Scroll down to end"

deferred class
	PF_HP_SUB_APPLICATION [C -> PF_HP_COMMAND create make end]

inherit
	EL_REGRESSION_TESTABLE_COMMAND_LINE_SUB_APPLICATION [C]
		redefine
			normal_run
		end

	EL_SHARED_CYCLIC_REDUNDANCY_CHECK_32

feature -- Basic operations

	normal_run
		do
			lio.set_timer
			Precursor
			lio.put_new_line
			lio.put_elapsed_time
			lio.put_new_line
		end

feature -- Tests

	test_run
		do
			Test.do_all_files_test ("pf_hp", "*.txt", agent test_sequence, checksum)
--			Test.do_file_test ("pf_hp/010011100100110.txt", agent test_sequence, 0)
--			Test.do_file_test ("pf_hp/01001001001.txt", agent test_sequence, 0)
		end

	test_sequence (output_path: EL_FILE_PATH)
		do
			test_sequence_with_checksum (
				output_path.without_extension.base, output_path.parent + (Folds_prefix + output_path.base),
				file_checksum (output_path)
			)
		end

	test_sequence_with_checksum (sequence: STRING; output_path: EL_FILE_PATH; sequence_checksum: NATURAL)
		local
			actual_checksum: NATURAL
		do
			log.enter_with_args ("test_sequence_with_checksum", << sequence >>)
			command := new_command (sequence, output_path)
			command.execute
			actual_checksum := file_checksum (command.output_path)
			if actual_checksum = sequence_checksum then
				lio.put_labeled_string ("Checksums agree", "OK")
			else
				lio.put_labeled_string ("Sequence checksum", sequence_checksum.out)
				lio.put_labeled_string (" Actual sequence checksum", actual_checksum.out)
			end
			lio.put_new_line
			log.exit
		end

feature {NONE} -- Implementation

	checksum: NATURAL
		-- regression testing checksum
		deferred
		end

	file_checksum (file_path: EL_FILE_PATH): NATURAL
		local
			crc: like crc_generator
		do
			crc := crc_generator
			crc.reset
			crc.add_file (file_path)
			Result := crc.checksum
		end

	argument_specs: ARRAY [like specs.item]
		do
			Result := <<
				valid_optional_argument ("sequence", "Input sequence", << ["Length must be >= 3", agent is_valid_sequence] >> ),
				optional_argument ("out", "Output file path")
			>>
		end

	default_make: PROCEDURE
		do
			Result := agent {PF_HP_COMMAND}.make (Default_sequence, default_output_path)
		end

	is_valid_sequence (sequence: READABLE_STRING_GENERAL): BOOLEAN
		do
			Result := sequence.count >= 3
		end

	default_output_path: EL_FILE_PATH
		do
			Result := Work_area_dir + option_name.to_string_8
		end

	new_command (sequence: STRING; output_path: EL_FILE_PATH): like command
		do
			create Result.make (sequence, output_path)
		end

feature {NONE} -- Constants

	Default_sequence: STRING = "0010010010010"

	Folds_prefix: ZSTRING
		once
			Result := "folds-"
		end

note
	license: "[
		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
	]"

end
