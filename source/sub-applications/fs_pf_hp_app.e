note
	description: "[
		PF_HP Ver 1.0: brute force proteinfolding in the 2D HP Model
		using class FOLD_SEQUENCE
	]"

	usage: "[
		pf_hp -fs_pf [-logging] [-sequence <protein sequence as binary number>] [-out <output path>]
	]"

	copyright: "[
		Copyright (C) 2016-2017  Gerrit Leder, Finnian Reilly

		Gerrit Leder, Overather Str. 10, 51429 Bergisch-Gladbach, GERMANY
		gerrit.leder@gmail.com

		Finnian Reilly, Dunboyne, Co Meath, Ireland.
		finnian@eiffel-loop.com
	]"

	license: "Scroll down to end"

class
	FS_PF_HP_APP

inherit
	PF_HP_SUB_APPLICATION [FS_PF_HP]
		redefine
			Option_name
		end

create
	make

feature {NONE} -- Constants

	Checksum: NATURAL = 0

	Description: STRING = "Test optimised distributed calculation of HP sequences in two-dimensional grid"

	Log_filter: ARRAY [like CLASS_ROUTINES]
			--
		do
			Result := <<
				[{FS_PF_HP_APP}, All_routines],
				[{FS_PF_HP}, All_routines]
			>>
		end

	Option_name: STRING = "fs_pf"

note
	license: "[
		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
	]"

end -- class PF_HP

