note
	description: "[
		Eiffel tests that can be executed by testing tool.
	]"
	author: "Gerrit Leder"
	testing: "type/manual"

	copyright: "[
		Copyright (C) 2016-2017  Gerrit Leder, Finnian Reilly

		Gerrit Leder, Overather Str. 10, 51429 Bergisch-Gladbach, GERMANY
		gerrit.leder@gmail.com

		Finnian Reilly, Dunboyne, Co Meath, Ireland.
		finnian@eiffel-loop.com
	]"

	license: "Scroll down to end"

class
	PF_HP_3_0_TEST_SET

inherit
	EQA_TEST_SET
		rename
			assert as assert_old
		redefine
			on_prepare

		end

	EQA_COMMONLY_USED_ASSERTIONS
		undefine
			default_create
		end

	EL_TEST_SET_BRIDGE
		undefine
			default_create
		end


	PF_HP_COMMAND

		rename
			File_system as File_system2
		undefine
			default_create
		end

	EL_MODULE_LOG
		undefine
			default_create
		end

	DIRECTION_ROUTINES_3_0
		undefine
			default_create
		end

--	POINT_SET_CONSTANTS
--		undefine
--			default_create
--		end


	BOOLEAN_CONSTANTS
		undefine
			default_create
		end

feature {PF_HP_3_0_TEST_SET} -- Events


	fold, fold1, fold2: FOLD_ARRAY_3_0

	--min : INTEGER

	--zero: BOOLEAN

	folds: FOLDS_LIST_3_0;

	-- strout: STRING

	on_prepare
			-- <Precursor>
		do
			create tool.make
			create folds.make
--			folds.compare_objects  (done in make)

				--grid.a.row_offset := -i
				--grid.a.column_offset := -i
				--grid.used.row_offset := -i
				--grid.used.column_offset := -i

				--Io.put_string ("i :" + i.out)
				--Io.put_new_line
				--Io.put_string ("grid.a.count :" + grid.a.count.out)
				--Io.put_new_line

		--	create fold.make (a_strseq)
		--	create fold1.make (a_strseq)
		--	create fold2.make (a_strseq)

			--zero := False
		--	create strout.make (1)
			minimum_loss := 9999
				--create a_seq.make (3)
		end

	tool: TOOL_3_0

feature -- Test routines


	generate_folds (grid: GRID_3_5; sequence: PF_BOOL_STRING; a_fold: FOLD_ARRAY_3_0; local_folds: FOLDS_LIST_3_0): FOLDS_LIST_3_0
		local
			a_grid: GRID_3_5
			tmp_folds: FOLDS_LIST_3_0
			a_loss: INTEGER
		do
			a_loss := 9999

			create tmp_folds.make
			tmp_folds.compare_objects --(not done in make)
			local_folds.compare_objects
			tmp_folds := local_folds.twin
			a_grid := grid.twin
			a_grid.embed (sequence, a_fold.area)
			a_loss := tool.calc_losses (a_grid, 0, sequence, a_fold)
				--grid.embed (0, 0, a_seq, fold)
				--a_loss := grid.calc_losses (0, 0, fold)

			tmp_folds := tool.compare_loss_min_add_fold (a_grid, a_loss, minimum_loss, a_fold, local_folds)
			if a_loss < minimum_loss then
				minimum_loss := a_loss
			end
			Result := tmp_folds
		end

    test_dummy do end

	test_routine2_010010010010
			-- New test routine
		note
			testing: "covers/{GRID}.embed"
		local
			i: INTEGER
			grid: GRID_3_5
			a_strseq: STRING
			a_seq: PF_BOOL_STRING

			--folds: LINKED_LIST[STRING]
		do

			--fold1 := "NENWNWSWSES"
			--fold2 := "NWNENESESWS"

			--fold1 := "SRLLRLLRLLR"
			--fold2 := "SLRRLRRLRRL"
			a_strseq := "010010010010"
			a_strseq.compare_objects

			create fold1.make (a_strseq)
			fold1.put(S.as_natural_8, 1)
			fold1.put(R.as_natural_8, 2)
			fold1.put(L.as_natural_8, 3)
			fold1.put(L.as_natural_8, 4)
			fold1.put(R.as_natural_8, 5)
			fold1.put(L.as_natural_8, 6)
			fold1.put(L.as_natural_8, 7)
			fold1.put(R.as_natural_8, 8)
			fold1.put(L.as_natural_8, 9)
			fold1.put(L.as_natural_8, 10)
			fold1.put(R.as_natural_8, 11)


			create fold2.make (a_strseq)
			fold2.put(S.as_natural_8, 1)
			fold2.put(L.as_natural_8, 2)
			fold2.put(R.as_natural_8, 3)
			fold2.put(R.as_natural_8, 4)
			fold2.put(L.as_natural_8, 5)
			fold2.put(R.as_natural_8, 6)
			fold2.put(R.as_natural_8, 7)
			fold2.put(L.as_natural_8, 8)
			fold2.put(R.as_natural_8, 9)
			fold2.put(R.as_natural_8, 10)
			fold2.put(L.as_natural_8, 11)





			a_seq := convert_str_bool_string(a_strseq)

			i := a_seq.count + 1
			create grid.make (a_seq)


			folds := generate_folds (grid, a_seq, fold1, folds)


			--assert ("min > 0", min.is_equal (0))
			assert ("folds.is_empty <> True", folds.is_empty = False)
			assert ("folds.has (fold1) <> True", folds.has (fold1) = True)
			assert ("grid.used_has_zero <> False", grid.used_has_zero = True)

			create grid.make (a_seq)


			folds := generate_folds (grid, a_seq, fold2, folds)


			--assert ("min > 0", min.is_equal (0))
			assert ("folds.is_empty <> True", folds.is_empty = False)
			assert ("folds.has (fold1) <> True", folds.has (fold1) = True)
			assert ("folds.has (fold2) <> True", folds.has (fold2) = True)
			assert ("grid.used_has_zero <> False", grid.used_has_zero = True)
		end

	test_routine2_1111
			-- New test routine
		note
			testing: "covers/{GRID}.embed"
		local
			i: INTEGER
			grid: GRID_3_5
			a_strseq: STRING
			a_seq: PF_BOOL_STRING
			a_loss: INTEGER
		do
			a_loss := 9999
			minimum_loss := 9999
			--create fold.make ("NWS")
			--create fold.make ("SLL")

			a_strseq := "1111"

			create fold.make (a_strseq)
			fold.put(S.as_natural_8, 1)
			fold.put(L.as_natural_8, 2)
			fold.put(L.as_natural_8, 3)


			a_seq := convert_str_bool_string(a_strseq)
			i := a_seq.count + 1
			create grid.make (a_seq)

			grid.embed (a_seq, fold.area)
			a_loss := grid.losses (fold.area)
				--grid.embed (0, 0, a_seq, fold)
				--a_loss := grid.calc_losses (0, 0, fold)
			-- strout := "a_seq " + a_strseq.to_string_8+ " with Fold "+ fold.to_string + " has a_loss: "
			--    io.put_string (strout)
			io.put_integer (a_loss)
			assert ("a_seq " + a_strseq.to_string_8+ " with Fold " +
			-- fold.to_string +
			 " has a_loss <> 8", a_loss.is_equal (8))
			folds := tool.compare_loss_min_add_fold (grid, a_loss, minimum_loss, fold, folds)
			if a_loss < minimum_loss then
				minimum_loss := a_loss
			end
			assert ("min <> 8", minimum_loss.is_equal (8))
			assert ("folds.is_empty <> True", folds.is_empty = False)
			assert ("folds.has (fold) <> True", folds.has (fold) = True)
			assert ("grid.used_has_zero <> False", grid.used_has_zero = True)
		end

	test_routine2_1001
			-- New test routine
		note
			testing: "covers/{GRID}.embed"
		local
			i: INTEGER
			grid: GRID_3_5
			a_strseq: STRING
			a_seq: PF_BOOL_STRING
			a_loss: INTEGER
		do
			a_loss := 9999
			minimum_loss := 9999
			--create fold.make ("NWS")
			--create fold.make ("SLL")

			a_strseq := "1001"

			create fold.make (a_strseq)
			fold.put(S.as_natural_8, 1)
			fold.put(L.as_natural_8, 2)
			fold.put(L.as_natural_8, 3)


			a_seq := convert_str_bool_string(a_strseq)
			i := a_seq.count + 1
			create grid.make (a_seq)

			grid.embed (a_seq, fold.area)
			a_loss := grid.losses (fold.area)
				--grid.embed (0, 0, a_seq, fold)
				--a_loss := grid.calc_losses (0, 0, fold)
			-- strout := "a_seq " + a_strseq.to_string_8+ " with Fold "+ fold.to_string + " has a_loss: "
			--    io.put_string (strout)
			io.put_integer (a_loss)
			assert ("a_seq " + a_strseq.to_string_8+ " with Fold " +
			-- fold.to_string +
			 " has a_loss <> 4", a_loss.is_equal (4))
			folds := tool.compare_loss_min_add_fold (grid, a_loss, minimum_loss, fold, folds)
			if a_loss < minimum_loss then
				minimum_loss := a_loss
			end
			assert ("min <> 4", minimum_loss.is_equal (4))
			assert ("folds.is_empty <> True", folds.is_empty = False)
			assert ("folds.has (fold) <> True", folds.has (fold) = True)
			assert ("grid.used_has_zero <> False", grid.used_has_zero = True)
		end

	test_routine2_0100
			-- New test routine
		note
			testing: "covers/{GRID}.embed"
		local
			i: INTEGER
			grid: GRID_3_5
			a_strseq: STRING
			a_seq: PF_BOOL_STRING
			a_loss: INTEGER
		do
			a_loss := 9999
			minimum_loss := 9999
			--create fold.make ("NWS")
			--create fold.make ("SLL")

			a_strseq := "0100"

			create fold.make (a_strseq)
			fold.put(S.as_natural_8, 1)
			fold.put(L.as_natural_8, 2)
			fold.put(L.as_natural_8, 3)


			a_seq := convert_str_bool_string(a_strseq)
			i := a_seq.count + 1
			create grid.make (a_seq)

			grid.embed (a_seq, fold.area)
			a_loss := grid.losses (fold.area)
				--grid.embed (0, 0, a_seq, fold)
				--a_loss := grid.calc_losses (0, 0, fold)
			-- strout := "a_seq " + a_strseq.to_string_8+ " with Fold "+ fold.to_string + " has a_loss: "
			--    io.put_string (strout)
			io.put_integer (a_loss)
			assert ("a_seq " + a_strseq.to_string_8+ " with Fold " +
			-- fold.to_string +
			 " has a_loss <> 2", a_loss.is_equal (2))
			folds := tool.compare_loss_min_add_fold (grid, a_loss, minimum_loss, fold, folds)
			if a_loss < minimum_loss then
				minimum_loss := a_loss
			end
			assert ("min <> 2", minimum_loss.is_equal (2))
			assert ("folds.is_empty <> True", folds.is_empty = False)
			assert ("folds.has (fold) <> True", folds.has (fold) = True)
			assert ("grid.used_has_zero <> False", grid.used_has_zero = True)
		end

	test_routine2_0010
			-- New test routine
		note
			testing: "covers/{GRID}.embed"
		local
			i: INTEGER
			grid: GRID_3_5
			a_strseq: STRING
			a_seq: PF_BOOL_STRING
			a_loss: INTEGER
		do
			a_loss := 9999
			minimum_loss := 9999
			--create fold.make ("NWS")
			--create fold.make ("SLL")

			a_strseq := "0010"

			create fold.make (a_strseq)
			fold.put(S.as_natural_8, 1)
			fold.put(L.as_natural_8, 2)
			fold.put(L.as_natural_8, 3)


			a_seq := convert_str_bool_string(a_strseq)
			i := a_seq.count + 1
			create grid.make (a_seq)

			grid.embed (a_seq, fold.area)
			a_loss := grid.losses (fold.area)
				--grid.embed (0, 0, a_seq, fold)
				--a_loss := grid.calc_losses (0, 0, fold)
			-- strout := "a_seq " + a_strseq.to_string_8+ " with Fold "+ fold.to_string + " has a_loss: "
			--    io.put_string (strout)
			io.put_integer (a_loss)
			assert ("a_seq " + a_strseq.to_string_8+ " with Fold " +
			-- fold.to_string +
			 " has a_loss <> 2", a_loss.is_equal (2))
			folds := tool.compare_loss_min_add_fold (grid, a_loss, minimum_loss, fold, folds)
			if a_loss < minimum_loss then
				minimum_loss := a_loss
			end
			assert ("min <> 2", minimum_loss.is_equal (2))
			assert ("folds.is_empty <> True", folds.is_empty = False)
			assert ("folds.has (fold) <> True", folds.has (fold) = True)
			assert ("grid.used_has_zero <> False", grid.used_has_zero = True)
		end

	test_routine2_0110
			-- New test routine
		note
			testing: "covers/{GRID}.embed"
		local
			i: INTEGER
			grid: GRID_3_5
			a_strseq: STRING
			a_seq: PF_BOOL_STRING
			a_loss: INTEGER
		do
			a_loss := 9999
			minimum_loss := 9999
			--create fold.make ("NWS")
			--create fold.make ("SLL")

			a_strseq := "0110"

			create fold.make (a_strseq)
			fold.put(S.as_natural_8, 1)
			fold.put(L.as_natural_8, 2)
			fold.put(L.as_natural_8, 3)


			a_seq := convert_str_bool_string(a_strseq)
			i := a_seq.count + 1
			create grid.make (a_seq)

			grid.embed (a_seq, fold.area)
			a_loss := grid.losses (fold.area)
				--grid.embed (0, 0, a_seq, fold)
				--a_loss := grid.calc_losses (0, 0, fold)
			-- strout := "a_seq " + a_strseq.to_string_8+ " with Fold "+ fold.to_string + " has a_loss: "
			--    io.put_string (strout)
			io.put_integer (a_loss)
			assert ("a_seq " + a_strseq.to_string_8+ " with Fold " +
			-- fold.to_string +
			 " has a_loss <> 4", a_loss.is_equal (4))
			folds := tool.compare_loss_min_add_fold (grid, a_loss, minimum_loss, fold, folds)
			if a_loss < minimum_loss then
				minimum_loss := a_loss
			end
			assert ("min <> 4", minimum_loss.is_equal (4))
			assert ("folds.is_empty <> True", folds.is_empty = False)
			assert ("folds.has (fold) <> True", folds.has (fold) = True)
			assert ("grid.used_has_zero <> False", grid.used_has_zero = True)
		end



	test_routine_000
			-- New test routine
		note
			testing: "covers/{GRID}.embed"
		local
			i: INTEGER
			grid: GRID_3_5
			a_strseq: STRING
			a_seq: PF_BOOL_STRING
			a_loss: INTEGER
		do
			a_loss := 9999
			minimum_loss := 9999
			a_strseq := "000"
			--create fold.make ("NN")
			--create fold.make ("SS")
			create fold.make (a_strseq)
			fold.put(S.as_natural_8, 1)
			fold.put(S.as_natural_8, 2)



			a_seq := convert_str_bool_string(a_strseq)
			i := a_seq.count + 1
			create grid.make (a_seq)

			grid.embed (a_seq, fold.area)
			a_loss := grid.losses (fold.area)
				--grid.embed (0, 0, a_seq, fold)
				--a_loss := grid.calc_losses (0, 0, fold)
			-- strout := "a_seq " + a_strseq.to_string_8+ " with Fold "+ fold.to_string + " has a_loss: "
			--    io.put_string (strout)
			io.put_integer (a_loss)
			assert ("a_seq " + a_strseq.to_string_8+ " with Fold " +
			-- fold.to_string +
			 " has a_loss <> 0", a_loss.is_equal (0))
			folds := tool.compare_loss_min_add_fold (grid, a_loss, minimum_loss, fold, folds)
			if a_loss < minimum_loss then
				minimum_loss := a_loss
			end
			assert ("min <> 0", minimum_loss.is_equal (0))
			assert ("folds.is_empty <> True", folds.is_empty = False)
			assert ("folds.has (fold) <> True", folds.has (fold) = True)
			assert ("grid.used_has_zero <> False", grid.used_has_zero = True)
		end



	test_routine_010010010010
			-- New test routine
		note
			testing: "covers/{GRID}.embed"
		local
			i: INTEGER
			grid: GRID_3_5
			a_strseq: STRING
			a_seq: PF_BOOL_STRING

			--folds: LINKED_LIST[STRING]
		do


			--fold2 := "NWNENESESWS"
			--fold2 := "SLRRLRRLRRL"

			a_strseq := "010010010010"

			create fold2.make (a_strseq)
			fold2.put(S.as_natural_8, 1)
			fold2.put(L.as_natural_8, 2)
			fold2.put(R.as_natural_8, 3)
			fold2.put(R.as_natural_8, 4)
			fold2.put(L.as_natural_8, 5)
			fold2.put(R.as_natural_8, 6)
			fold2.put(R.as_natural_8, 7)
			fold2.put(L.as_natural_8, 8)
			fold2.put(R.as_natural_8, 9)
			fold2.put(R.as_natural_8, 10)
			fold2.put(L.as_natural_8, 11)


			a_seq := convert_str_bool_string(a_strseq)
			i := a_seq.count + 1
			create grid.make (a_seq)



			folds := generate_folds (grid, a_seq, fold2, folds)


			--assert ("min > 0", min.is_equal (0))
			assert ("folds.is_empty <> True", folds.is_empty = False)
			assert ("folds.has (fold2) <> True", folds.has (fold2) = True)
			assert ("grid.used_has_zero <> False", grid.used_has_zero = True)
		end



	test_routine_10010010010
			-- New test routine
		note
			testing: "covers/{GRID}.embed"
		local
			i: INTEGER
			grid: GRID_3_5
			a_strseq: STRING
			a_seq: PF_BOOL_STRING
			a_loss: INTEGER
		do
			a_loss := 9999
			minimum_loss := 9999

			--create fold.make ("NWSWSESENE")
			--create fold.make ("SLLRLLRLLR")

			a_strseq := "10010010010"

			create fold.make (a_strseq)
			fold.put(S.as_natural_8, 1)
			fold.put(L.as_natural_8, 2)
			fold.put(L.as_natural_8, 3)
			fold.put(R.as_natural_8, 4)
			fold.put(L.as_natural_8, 5)
			fold.put(L.as_natural_8, 6)
			fold.put(R.as_natural_8, 7)
			fold.put(L.as_natural_8, 8)
			fold.put(L.as_natural_8, 9)
			fold.put(R.as_natural_8, 10)



			a_seq := convert_str_bool_string(a_strseq)
			i := a_seq.count + 1
			create grid.make (a_seq)

			grid.embed (a_seq, fold.area)
			a_loss := grid.losses (fold.area)
				--grid.embed (0, 0, a_seq, fold)
				--a_loss := grid.calc_losses (0, 0, fold)
			-- strout := "a_seq " + a_strseq.to_string_8+ " with Fold "+ fold.to_string + " has a_loss: "
			--    io.put_string (strout)
			io.put_integer (a_loss)
			assert ("a_seq " + a_strseq.to_string_8 + " with Fold " +
			--fold.to_string +
			 " has a_loss <> 1", a_loss.is_equal (1))
			folds := tool.compare_loss_min_add_fold (grid, a_loss, minimum_loss, fold, folds)
			if a_loss < minimum_loss then
				minimum_loss := a_loss
			end
			assert ("min <> 1", minimum_loss.is_equal (1))
			assert ("folds.is_empty <> True", folds.is_empty = False)
			assert ("folds.has (fold) <> True", folds.has (fold) = True)
			assert ("grid.used_has_zero <> False", grid.used_has_zero = True)
		end

	test_routine_111
			-- New test routine
		note
			testing: "covers/{GRID}.embed"
		local
			i: INTEGER
			grid: GRID_3_5
			a_strseq: STRING
			a_seq: PF_BOOL_STRING
			a_loss: INTEGER
		do
			a_loss := 9999
			minimum_loss := 9999
			--create fold.make ("NN")
			--create fold.make ("SS")

			a_strseq := "111"

			create fold.make (a_strseq)
			fold.put(S.as_natural_8, 1)
			fold.put(S.as_natural_8, 2)


			a_seq := convert_str_bool_string(a_strseq) --fold is not set to a_strseq.count*"N" -> removed!
			i := a_seq.count + 1
			create grid.make (a_seq)

			grid.embed (a_seq, fold.area)
			a_loss := grid.losses (fold.area)
				--grid.embed (0, 0, a_seq, fold)
				--a_loss := grid.calc_losses (0, 0, fold)
			-- strout := "a_seq " + a_strseq.to_string_8 + " with Fold " + fold.to_string + " has a_loss: "
			--    io.put_string (strout)
			io.put_integer (a_loss)
			assert ("a_seq " + a_strseq.to_string_8 + " with Fold " +
			-- fold.to_string +
			 " has a_loss <> 8", a_loss.is_equal (8))
			folds := tool.compare_loss_min_add_fold (grid, a_loss, minimum_loss, fold, folds)
			if a_loss < minimum_loss then
				minimum_loss := a_loss
			end
			assert ("min <> 8", minimum_loss.is_equal (8))
			assert ("folds.is_empty <> True", folds.is_empty = False)
			assert ("folds.has (fold) <> True", folds.has (fold) = True)
			assert ("grid.used_has_zero <> False", grid.used_has_zero = True)
		end

	test_routine_1111
			-- New test routine
		note
			testing: "covers/{GRID}.embed"
		local
			i: INTEGER
			grid: GRID_3_5
			a_strseq: STRING
			a_seq: PF_BOOL_STRING
			a_loss: INTEGER
		do
			a_loss := 9999
			minimum_loss := 9999
			--create fold.make ("NNN")
			--fold.make (3)



			a_strseq := "1111"
			a_seq := convert_str_bool_string(a_strseq) --fold is not set to a_strseq.count*"N" -> removed!

			create fold.make (a_strseq)
			fold.put(S.as_natural_8, 1)
			fold.put(S.as_natural_8, 2)
			fold.put(S.as_natural_8, 3)

			i := a_seq.count + 1
			create grid.make (a_seq)

			grid.embed (a_seq, fold.area)
			a_loss := grid.losses (fold.area)
				--grid.embed (0, 0, a_seq, fold)
				--a_loss := grid.calc_losses (0, 0, fold)
			-- strout := "a_seq " + a_strseq.to_string_8 + " with Fold " + fold.do_all (agent (char: NATURAL_8) do print(char + "; ") end)  + " has a_loss: "
			--    io.put_string (strout)
			io.put_integer (a_loss)
			assert ("a_seq " + a_strseq.to_string_8 + " with Fold " +
			-- fold.to_string +
			 " has a_loss <> 10", a_loss.is_equal (10))
			folds := tool.compare_loss_min_add_fold (grid, a_loss, minimum_loss, fold, folds)
			if a_loss < minimum_loss then
				minimum_loss := a_loss
			end
			assert ("min <> 10", minimum_loss.is_equal (10))
			assert ("folds.is_empty <> True", folds.is_empty = False)
			assert ("folds.has (fold) <> True", folds.has (fold) = True)
			assert ("grid.used_has_zero <> False", grid.used_has_zero = True)
		end

	test_routine_1000
			-- New test routine
		note
			testing: "covers/{GRID}.embed"
		local
			i: INTEGER
			grid: GRID_3_5
			a_strseq: STRING
			a_seq: PF_BOOL_STRING
			a_loss: INTEGER
		do
			a_loss := 9999
			minimum_loss := 9999
			--create fold.make ("NNN")
			--fold.make (3)



			a_strseq := "1000"
			a_seq := convert_str_bool_string(a_strseq) --fold is not set to a_strseq.count*"N" -> removed!

			create fold.make (a_strseq)
			fold.put(S.as_natural_8, 1)
			fold.put(S.as_natural_8, 2)
			fold.put(S.as_natural_8, 3)

			i := a_seq.count + 1
			create grid.make (a_seq)

			grid.embed (a_seq, fold.area)
			a_loss := grid.losses (fold.area)
				--grid.embed (0, 0, a_seq, fold)
				--a_loss := grid.calc_losses (0, 0, fold)
			-- strout := "a_seq " + a_strseq.to_string_8 + " with Fold " + fold.do_all (agent (char: NATURAL_8) do print(char + "; ") end)  + " has a_loss: "
			--    io.put_string (strout)
			io.put_integer (a_loss)
			assert ("a_seq " + a_strseq.to_string_8 + " with Fold " +
			-- fold.to_string +
			 " has a_loss <> 3", a_loss.is_equal (3))
			folds := tool.compare_loss_min_add_fold (grid, a_loss, minimum_loss, fold, folds)
			if a_loss < minimum_loss then
				minimum_loss := a_loss
			end
			assert ("min <> 3", minimum_loss.is_equal (3))
			assert ("folds.is_empty <> True", folds.is_empty = False)
			assert ("folds.has (fold) <> True", folds.has (fold) = True)
			assert ("grid.used_has_zero <> False", grid.used_has_zero = True)
		end

	test_routine_0001
			-- New test routine
		note
			testing: "covers/{GRID}.embed"
		local
			i: INTEGER
			grid: GRID_3_5
			a_strseq: STRING
			a_seq: PF_BOOL_STRING
			a_loss: INTEGER
		do
			a_loss := 9999
			minimum_loss := 9999
			--create fold.make ("NNN")
			--fold.make (3)



			a_strseq := "0001"
			a_seq := convert_str_bool_string(a_strseq) --fold is not set to a_strseq.count*"N" -> removed!

			create fold.make (a_strseq)
			fold.put(S.as_natural_8, 1)
			fold.put(S.as_natural_8, 2)
			fold.put(S.as_natural_8, 3)

			i := a_seq.count + 1
			create grid.make (a_seq)

			grid.embed (a_seq, fold.area)
			a_loss := grid.losses (fold.area)
				--grid.embed (0, 0, a_seq, fold)
				--a_loss := grid.calc_losses (0, 0, fold)
			-- strout := "a_seq " + a_strseq.to_string_8 + " with Fold " + fold.do_all (agent (char: NATURAL_8) do print(char + "; ") end)  + " has a_loss: "
			--    io.put_string (strout)
			io.put_integer (a_loss)
			assert ("a_seq " + a_strseq.to_string_8 + " with Fold " +
			-- fold.to_string +
			 " has a_loss <> 3", a_loss.is_equal (3))
			folds := tool.compare_loss_min_add_fold (grid, a_loss, minimum_loss, fold, folds)
			if a_loss < minimum_loss then
				minimum_loss := a_loss
			end
			assert ("min <> 3", minimum_loss.is_equal (3))
			assert ("folds.is_empty <> True", folds.is_empty = False)
			assert ("folds.has (fold) <> True", folds.has (fold) = True)
			assert ("grid.used_has_zero <> False", grid.used_has_zero = True)
		end

	test_routine_0100
			-- New test routine
		note
			testing: "covers/{GRID}.embed"
		local
			i: INTEGER
			grid: GRID_3_5
			a_strseq: STRING
			a_seq: PF_BOOL_STRING
			a_loss: INTEGER
		do
			a_loss := 9999
			minimum_loss := 9999
			--create fold.make ("NNN")
			--fold.make (3)



			a_strseq := "0100"
			a_seq := convert_str_bool_string(a_strseq) --fold is not set to a_strseq.count*"N" -> removed!

			create fold.make (a_strseq)
			fold.put(S.as_natural_8, 1)
			fold.put(S.as_natural_8, 2)
			fold.put(S.as_natural_8, 3)

			i := a_seq.count + 1
			create grid.make (a_seq)

			grid.embed (a_seq, fold.area)
			a_loss := grid.losses (fold.area)
				--grid.embed (0, 0, a_seq, fold)
				--a_loss := grid.calc_losses (0, 0, fold)
			-- strout := "a_seq " + a_strseq.to_string_8 + " with Fold " + fold.do_all (agent (char: NATURAL_8) do print(char + "; ") end)  + " has a_loss: "
			--    io.put_string (strout)
			io.put_integer (a_loss)
			assert ("a_seq " + a_strseq.to_string_8 + " with Fold " +
			-- fold.to_string +
			 " has a_loss <> 2", a_loss.is_equal (2))
			folds := tool.compare_loss_min_add_fold (grid, a_loss, minimum_loss, fold, folds)
			if a_loss < minimum_loss then
				minimum_loss := a_loss
			end
			assert ("min <> 2", minimum_loss.is_equal (2))
			assert ("folds.is_empty <> True", folds.is_empty = False)
			assert ("folds.has (fold) <> True", folds.has (fold) = True)
			assert ("grid.used_has_zero <> False", grid.used_has_zero = True)
		end

	test_routine_0010
			-- New test routine
		note
			testing: "covers/{GRID}.embed"
		local
			i: INTEGER
			grid: GRID_3_5
			a_strseq: STRING
			a_seq: PF_BOOL_STRING
			a_loss: INTEGER
		do
			a_loss := 9999
			minimum_loss := 9999
			--create fold.make ("NNN")
			--fold.make (3)



			a_strseq := "0010"
			a_seq := convert_str_bool_string(a_strseq) --fold is not set to a_strseq.count*"N" -> removed!

			create fold.make (a_strseq)
			fold.put(S.as_natural_8, 1)
			fold.put(S.as_natural_8, 2)
			fold.put(S.as_natural_8, 3)

			i := a_seq.count + 1
			create grid.make (a_seq)

			grid.embed (a_seq, fold.area)
			a_loss := grid.losses (fold.area)
				--grid.embed (0, 0, a_seq, fold)
				--a_loss := grid.calc_losses (0, 0, fold)
			-- strout := "a_seq " + a_strseq.to_string_8 + " with Fold " + fold.do_all (agent (char: NATURAL_8) do print(char + "; ") end)  + " has a_loss: "
			--    io.put_string (strout)
			io.put_integer (a_loss)
			assert ("a_seq " + a_strseq.to_string_8 + " with Fold " +
			-- fold.to_string +
			 " has a_loss <> 2", a_loss.is_equal (2))
			folds := tool.compare_loss_min_add_fold (grid, a_loss, minimum_loss, fold, folds)
			if a_loss < minimum_loss then
				minimum_loss := a_loss
			end
			assert ("min <> 2", minimum_loss.is_equal (2))
			assert ("folds.is_empty <> True", folds.is_empty = False)
			assert ("folds.has (fold) <> True", folds.has (fold) = True)
			assert ("grid.used_has_zero <> False", grid.used_has_zero = True)
		end



feature {NONE} -- Implementation

	convert_str_bool_string (a_strseq: STRING): PF_BOOL_STRING
			--Given a bit string of values '0's and '1's return a sequence of values False and True
		local
			i: INTEGER
		do
			create Result.make (a_strseq.count)
			from
				i := 1
			until
				i = a_strseq.count
			loop
				i := i + 1
			end
			from
				i := 1
			until
				i = a_strseq.count + 1
			loop
				if a_strseq.item (i).is_equal ('1') then
					Result.put (True, i)
				else
					Result.put (False, i)
				end
				i := i + 1
			end
		end

	gen_folds
		do
		end

	print_min_fold
		do
		end

feature {NONE} -- Internal attributes

	seq: PF_BOOL_STRING;

note
	license: "[
		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
	]"

end
