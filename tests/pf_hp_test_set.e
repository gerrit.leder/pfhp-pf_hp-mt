note
	description: "[
		Eiffel tests that can be executed by testing tool.
	]"
	author: "Gerrit Leder"

	copyright: "[
		Copyright (C) 2016-2017  Gerrit Leder, Finnian Reilly

		Gerrit Leder, Overather Str. 10, 51429 Bergisch-Gladbach, GERMANY
		gerrit.leder@gmail.com

		Finnian Reilly, Dunboyne, Co Meath, Ireland.
		finnian@eiffel-loop.com
	]"

	license: "Scroll down to end"

class
	PF_HP_TEST_SET

inherit

	EQA_TEST_SET
		redefine
			on_prepare
		end

feature {PF_HP_TEST_SET} -- Events

	tool: TOOL_1_0

	fold, fold1, fold2, s: STRING

	min : INTEGER

	zero: BOOLEAN

	folds: LINKED_LIST [STRING];

	on_prepare
			-- <Precursor>
		do
			create tool.make
			create folds.make
			folds.compare_objects

				--grid.a.row_offset := -i
				--grid.a.column_offset := -i
				--grid.used.row_offset := -i
				--grid.used.column_offset := -i

				--Io.put_string ("i :" + i.out)
				--Io.put_new_line
				--Io.put_string ("grid.a.count :" + grid.a.count.out)
				--Io.put_new_line

			fold := ""
			fold1 := ""
			fold2 := ""

			zero := False
			create s.make (1)
			min := 9999
				--create seq.make (3)
		end

feature -- Test routines

	test_routine_000
			-- New test routine
		note
			testing: "covers/{GRID}.embed"
		local
			i: INTEGER
			grid: GRID_1_0
			strseq: STRING
			seq: PF_BOOL_STRING
			loss: INTEGER
		do
			loss := 9999
			min := 9999
			fold := "NN"
			strseq := "000"
			seq := tool.convert_str_bool_string (strseq)
			i := seq.count + 1
			create grid.make (i)
			check_grid (grid, i)
			grid := tool.embed (grid, 0, seq, fold)
			loss := tool.calc_losses (grid, 0, seq, fold)
				--grid.embed (0, 0, seq, fold)
				--loss := grid.calc_losses (0, 0, fold)
			s := "Seq " + strseq + " with Fold " + fold + " has loss: "
			io.put_string (s)
			io.put_integer (loss)
			assert ("Seq " + strseq + " with Fold " + fold + " has loss <> 0", loss.is_equal (0))
			folds := tool.compare_loss_min_add_fold (grid, loss, min, fold, folds)
			if loss < min then
				min := loss
			end
			assert ("min <> 0", min.is_equal (0))
			assert ("folds.is_empty <> True", folds.is_empty = False)
			assert ("folds.has (fold) <> True", folds.has (fold) = True)
			assert ("grid.used.has (zero) <> False", grid.used.has (zero) = True)
		end

	test_routine2_000
			-- New test routine
		note
			testing: "covers/{GRID}.embed"
		local
			i: INTEGER
			grid: GRID_1_0
			strseq: STRING
			seq: PF_BOOL_STRING
			loss: INTEGER
		do
			loss := 9999
			min := 9999
			create folds.make
			fold := "NS"
			strseq := "000"
			min := 9999
			seq := tool.convert_str_bool_string (strseq)
			i := seq.count + 1
			create grid.make (i)
			check_grid (grid, i)
			grid := tool.embed (grid, 0, seq, fold)
			loss := tool.calc_losses (grid, 0, seq, fold)
				--grid.embed (0, 0, seq, fold)
				--loss := grid.calc_losses (0, 0, fold)
			s := "Seq " + strseq + " with Fold " + fold + " has loss: "
			io.put_string (s)
			io.put_integer (loss)
			assert ("Seq " + strseq + " with Fold " + fold + " has loss <> 9999", loss.is_equal (9999))
			folds := tool.compare_loss_min_add_fold (grid, loss, min, fold, folds)
			if loss < min then
				min := loss
			end
			assert ("folds.is_empty <> True", folds.is_empty = True)
			assert ("min <> 9999", min.is_equal (9999))
			assert ("grid.used.has (zero) <> False", grid.used.has (zero) = False)
		end

	test_routine_111
			-- New test routine
		note
			testing: "covers/{GRID}.embed"
		local
			i: INTEGER
			grid: GRID_1_0
			strseq: STRING
			seq: PF_BOOL_STRING
			loss: INTEGER
		do
			loss := 9999
			min := 9999
			fold := "NN"
			strseq := "111"
			seq := tool.convert_str_bool_string (strseq) --fold is not set to strseq.count*"N" -> removed!
			i := seq.count + 1
			create grid.make (i)
			check_grid (grid, i)
			grid := tool.embed (grid, 0, seq, fold)
			loss := tool.calc_losses (grid, 0, seq, fold)
				--grid.embed (0, 0, seq, fold)
				--loss := grid.calc_losses (0, 0, fold)
			s := "Seq " + strseq + " with Fold " + fold + " has loss: "
			io.put_string (s)
			io.put_integer (loss)
			assert ("Seq " + strseq + " with Fold " + fold + " has loss <> 8", loss.is_equal (8))
			folds := tool.compare_loss_min_add_fold (grid, loss, min, fold, folds)
			if loss < min then
				min := loss
			end
			assert ("min <> 8", min.is_equal (8))
			assert ("folds.is_empty <> True", folds.is_empty = False)
			assert ("folds.has (fold) <> True", folds.has (fold) = True)
			assert ("grid.used.has (zero) <> False", grid.used.has (zero) = True)
		end

	test_routine_1111
			-- New test routine
		note
			testing: "covers/{GRID}.embed"
		local
			i: INTEGER
			grid: GRID_1_0
			strseq: STRING
			seq: PF_BOOL_STRING
			loss: INTEGER
		do
			loss := 9999
			min := 9999
			fold := "NNN"
			strseq := "1111"
			seq := tool.convert_str_bool_string (strseq) --fold is not set to strseq.count*"N" -> removed!

			i := seq.count + 1
			create grid.make (i)
			check_grid (grid, i)
			grid := tool.embed (grid, 0, seq, fold)
			loss := tool.calc_losses (grid, 0, seq, fold)
				--grid.embed (0, 0, seq, fold)
				--loss := grid.calc_losses (0, 0, fold)
			s := "Seq " + strseq + " with Fold " + fold + " has loss: "
			io.put_string (s)
			io.put_integer (loss)
			assert ("Seq " + strseq + " with Fold " + fold + " has loss <> 10", loss.is_equal (10))
			folds := tool.compare_loss_min_add_fold (grid, loss, min, fold, folds)
			if loss < min then
				min := loss
			end
			assert ("min <> 10", min.is_equal (10))
			assert ("folds.is_empty <> True", folds.is_empty = False)
			assert ("folds.has (fold) <> True", folds.has (fold) = True)
			assert ("grid.used.has (zero) <> False", grid.used.has (zero) = True)
		end

	test_routine2_1111
			-- New test routine
		note
			testing: "covers/{GRID}.embed"
		local
			i: INTEGER
			grid: GRID_1_0
			strseq: STRING
			seq: PF_BOOL_STRING
			loss: INTEGER
		do
			loss := 9999
			min := 9999
			fold := "NWS"
			strseq := "1111"
			seq := tool.convert_str_bool_string (strseq)
			i := seq.count + 1
			create grid.make (i)
			check_grid (grid, i)
			grid := tool.embed (grid, 0, seq, fold)
			loss := tool.calc_losses (grid, 0, seq, fold)
				--grid.embed (0, 0, seq, fold)
				--loss := grid.calc_losses (0, 0, fold)
			s := "Seq " + strseq + " with Fold " + fold + " has loss: "
			io.put_string (s)
			io.put_integer (loss)
			assert ("Seq " + strseq + " with Fold " + fold + " has loss <> 8", loss.is_equal (8))
			folds := tool.compare_loss_min_add_fold (grid, loss, min, fold, folds)
			if loss < min then
				min := loss
			end
			assert ("min <> 8", min.is_equal (8))
			assert ("folds.is_empty <> True", folds.is_empty = False)
			assert ("folds.has (fold) <> True", folds.has (fold) = True)
			assert ("grid.used.has (zero) <> False", grid.used.has (zero) = True)
		end

	test_routine_10010010010
			-- New test routine
		note
			testing: "covers/{GRID}.embed"
		local
			i: INTEGER
			grid: GRID_1_0
			strseq: STRING
			seq: PF_BOOL_STRING
			loss: INTEGER
		do
			loss := 9999
			min := 9999
			fold := "NWSWSESENE"
			strseq := "10010010010"
			seq := tool.convert_str_bool_string (strseq)
			i := seq.count + 1
			create grid.make (i)
			check_grid (grid, i)
			grid := tool.embed (grid, 0, seq, fold)
			loss := tool.calc_losses (grid, 0, seq, fold)
				--grid.embed (0, 0, seq, fold)
				--loss := grid.calc_losses (0, 0, fold)
			s := "Seq " + strseq + " with Fold " + fold + " has loss: "
			io.put_string (s)
			io.put_integer (loss)
			assert ("Seq " + strseq + " with Fold " + fold + " has loss <> 1", loss.is_equal (1))
			folds := tool.compare_loss_min_add_fold (grid, loss, min, fold, folds)
			if loss < min then
				min := loss
			end
			assert ("min <> 1", min.is_equal (1))
			assert ("folds.is_empty <> True", folds.is_empty = False)
			assert ("folds.has (fold) <> True", folds.has (fold) = True)
			assert ("grid.used.has (zero) <> False", grid.used.has (zero) = True)
		end

	test_routine2_010010010010
			-- New test routine
		note
			testing: "covers/{GRID}.embed"
		local
			i: INTEGER
			grid: GRID_1_0
			strseq: STRING
			seq: PF_BOOL_STRING

			--folds: LINKED_LIST[STRING]
		do

			fold1 := "NENWNWSWSES"
			fold2 := "NWNENESESWS"

			strseq := "010010010010"
			strseq.compare_objects

			seq := tool.convert_str_bool_string (strseq)

			i := seq.count + 1
			create grid.make (i)
			check_grid (grid, i)

			folds := generate_folds (grid, seq, fold1, folds)


			--assert ("min > 0", min.is_equal (0))
			assert ("folds.is_empty <> True", folds.is_empty = False)
			assert ("folds.has (fold1) <> True", folds.has (fold1) = True)
			assert ("grid.used.has (zero) <> False", grid.used.has (zero) = True)

			create grid.make (i)
			check_grid (grid, i)

			folds := generate_folds (grid, seq, fold2, folds)


			--assert ("min > 0", min.is_equal (0))
			assert ("folds.is_empty <> True", folds.is_empty = False)
			assert ("folds.has (fold1) <> True", folds.has (fold1) = True)
			assert ("folds.has (fold2) <> True", folds.has (fold2) = True)
			assert ("grid.used.has (zero) <> False", grid.used.has (zero) = True)
		end

	test_routine_010010010010
			-- New test routine
		note
			testing: "covers/{GRID}.embed"
		local
			i: INTEGER
			grid: GRID_1_0
			strseq: STRING
			seq: PF_BOOL_STRING

			--folds: LINKED_LIST[STRING]
		do


			fold2 := "NWNENESESWS"

			strseq := "010010010010"
			seq := tool.convert_str_bool_string (strseq)
			i := seq.count + 1
			create grid.make (i)
			check_grid (grid, i)


			folds := generate_folds (grid, seq, fold2, folds)


			--assert ("min > 0", min.is_equal (0))
			assert ("folds.is_empty <> True", folds.is_empty = False)
			assert ("folds.has (fold2) <> True", folds.has (fold2) = True)
			assert ("grid.used.has (zero) <> False", grid.used.has (zero) = True)
		end



	check_grid (a_grid: GRID_1_0; an_i: INTEGER)
		do
			assert ("a not squared", a_grid.a.count = (an_i + an_i + 1) * (an_i + an_i + 1))
			assert ("used not squared", a_grid.used.count = (an_i + an_i + 1) * (an_i + an_i + 1))
		end

	generate_folds (grid: GRID_1_0; seq: PF_BOOL_STRING; a_fold: STRING; local_folds: LINKED_LIST [STRING]): LINKED_LIST [STRING]
		local
			a_grid: GRID_1_0
			tmp_folds: LINKED_LIST [STRING]
			loss: INTEGER
		do
			loss := 9999

			create tmp_folds.make
			tmp_folds.compare_objects
			tmp_folds := local_folds.twin
			a_grid := grid.twin
			a_grid := tool.embed (a_grid, 0, seq, a_fold)
			loss := tool.calc_losses (a_grid, 0, seq, a_fold)
				--grid.embed (0, 0, seq, fold)
				--loss := grid.calc_losses (0, 0, fold)

			tmp_folds := tool.compare_loss_min_add_fold (a_grid, loss, min, a_fold, local_folds)
			if loss < min then
				min := loss
			end
			Result := tmp_folds
		end
note
	license: "[
		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
	]"

end
