note
	description: "Fold array that updates CRC checksum in `calc_losses'"
	author: "Finnian Reilly"

	copyright: "[
		Copyright (C) 2016-2017  Gerrit Leder, Finnian Reilly

		Gerrit Leder, Overather Str. 10, 51429 Bergisch-Gladbach, GERMANY
		gerrit.leder@gmail.com

		Finnian Reilly, Dunboyne, Co Meath, Ireland.
		finnian@eiffel-loop.com
	]"

	license: "Scroll down to end"

class
	TESTABLE_FOLD_ARRAY

inherit
	FOLD_ARRAY
		redefine
			make, calc_losses
		end

create
	make

feature {NONE} -- Initialization

	make (strseq: STRING)
		do
			Precursor (strseq)
			create crc
		end

feature -- Access

	checksum: NATURAL
		do
			Result := crc.checksum
		end

feature {NONE} -- Implementation

	calc_losses (pf: PF_HP_2_0 [GRID_2_X]; iteration_count: NATURAL_32)
		do
			update_crc (crc)
		end

feature {NONE} -- Internal attributes

	crc: EL_CYCLIC_REDUNDANCY_CHECK_32;

note
	license: "[
		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
	]"

end
