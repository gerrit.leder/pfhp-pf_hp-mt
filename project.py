# EiffelStudio project environment

from eiffel_loop.eiffel.dev_environ import *
from eiffel_loop.os import path

version = (3, 1, 0); build = 2

installation_sub_directory = 'PF_HP'

tests = None
